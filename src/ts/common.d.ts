/// <reference path="../../typings/tsd.d.ts" />
/// <reference path="main.ts" />

/// <reference path="modules/restApi/restApi.d.ts" />

interface IUser {
	
}


interface IBook {
	id: number;
	name: string;
	price: number;
	img: string;
}