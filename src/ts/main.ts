/// <reference path="common.d.ts" />


var booksApp = angular.module('booksApp', ["ngRoute", 'ngMaterial', 'restApi']);

booksApp.config(['$routeProvider', '$mdThemingProvider',
  ($routeProvider, $mdThemingProvider) => {
    $routeProvider.
      when('/main', {
        controller: 'AuthController',
        controllerAs: 'vm'
      }).
      otherwise({
        redirectTo: '/main'
      });

    $mdThemingProvider.theme('default')
      .dark();

  }]);
  

 

