/// <reference path="../../../common.d.ts" />
module Users {
	export class UsersServices {

		private http: ng.IHttpService;
		constructor($http: ng.IHttpService) {
			this.http = $http;
		}

		public getUsers = (): Promise<Array<IUser>> => {
			return new Promise((onSuccess, onError) => {
				this.http.get('http://vps217222.ovh.net:8080/api/users').success((response: IRestApiResponse, status) => {
					if (response.success) {
						onSuccess(response.data);
					} else {
						onError(response.error);
					}
				}).error(error => {
					onError();
				});
			})
		}

		public addUser = (name: string, password: string): Promise<IUser> => {
			return new Promise((onSuccess, onError) => {
				this.http.post('http://vps217222.ovh.net:8080/api/users', {
					name: name,
					password: password
				}).success((response: IRestApiResponse, status) => {
					if (response.success) {
						onSuccess(response.data);
					} else {
						onError(response.error);
					}
				}).error(error => {
					onError();
				});
			})
		}
	}
}

booksApp.factory("UsersServices", ["$http", ($http) => new Users.UsersServices($http)]);
	
