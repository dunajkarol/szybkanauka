var test;
module RestApi {
    var config: RestApi.IConfig = {
        host: 'http://vps217222.ovh.net:8080',
        token: {
            name: 'token',
            header: 'x-access-token'            
        }
    };
    
    config.url = config.host + '/api';
    
    var getConfig = ():RestApi.IConfig => { 
        console.log("RestApiModule: getConfig")
        return config
    };

    angular.module('restApi', [])
        .constant('restApiConfig', getConfig())
        .factory('RestApiService', (restApiConfig:RestApi.IConfig, $http: ng.IHttpService) => new RestApi.RestApiService(restApiConfig, $http))
        .factory('AuthService', (RestApiService: RestApi.RestApiService, $q: ng.IQService, restApiConfig: RestApi.IConfig) => { 
            return new RestApi.AuthService(RestApiService, $q, config)
        })
        .controller('AuthController', (AuthService: RestApi.AuthService) => { 
            return new RestApi.AuthController(AuthService)
        })
        .run(['AuthService', (test: RestApi.AuthService) => { test.login("Nick Cerminara", "password").then((r) => {
            console.log(r);
        })}])
}