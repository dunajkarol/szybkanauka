/// <reference path="../../../common.d.ts" />

module RestApi {
	export class RestApiService {

		private $http: ng.IHttpService;
		private config: RestApi.IConfig;

		constructor(config: RestApi.IConfig, $http: ng.IHttpService) {
			this.$http = $http;
			this.config = config;
		}

		private getUrlMethod = (method: string): string => {
			if (!this.config) {
				throw "Brak konfiguracji";
			}

			return this.config.url + "/" + method;
		}

		private getHeaders = (): ng.IHttpRequestConfigHeaders => {
			var obj: ng.IHttpRequestConfigHeaders = {
				'Content-Type': 'application/x-www-form-urlencoded'
			};
			
			//ustwaienie tokenu
			var token = this.getToken();
			if (token)
				obj[this.config.token.header] = this.getToken();
			return obj;
		}

		private getToken = (): string => {
			return localStorage.getItem(this.config.token.name);
		}

		private getHttpRequest = (sendMthond: string, apiMethod: string, data?:Object): ng.IRequestConfig => {
			var request: ng.IRequestConfig = {
				method: sendMthond,
				url: this.getUrlMethod(apiMethod),
				//data: $.param(data),
				headers: this.getHeaders()
			}
			
			if(typeof data !== 'undefined' && data !== null)
				request.data = $.param(data);
			
			return request;
		}


		public getData = (method: string): ng.IPromise<RestApi.IResponse> => this.$http(this.getHttpRequest('GET', method)).then((response) => {
			return response.data;
		});

		public postData = (method: string, data: Object): ng.IPromise<RestApi.IResponse> => this.$http(this.getHttpRequest('POST', method, data)).then((response) => {
			return response.data;
		});;

		public putData = (method: string, data: Object): ng.IPromise<RestApi.IResponse> => this.$http(this.getHttpRequest('PUT', method, data)).then((response) => {
			return response.data;
		});;

		public deleteData = (method: string): ng.IPromise<RestApi.IResponse> => this.$http(this.getHttpRequest('DELETE', method)).then((response) => {
			return response.data;
		});;


	}
}