/// <reference path="../../../common.d.ts" />

module RestApi {
	export class AuthService {

		private restApiService: RestApi.RestApiService;
		private $q: ng.IQService;
		private config: RestApi.IConfig;
		
		constructor(restApiService: any, $q: ng.IQService, config: RestApi.IConfig) {
			this.restApiService = restApiService;
			this.$q = $q;
			this.config = config;
		}

		public login = (name:string, password: string): ng.IPromise<RestApi.ResponseData.ILogin> => new this.$q((onSuccess, onError) => {
			this.restApiService.postData("login", {name: name,password: password}).then((response: IResponseLogin) => {
				if(response.success){		
					this.saveToken(response.data.token);			
					onSuccess(response.data);
				}else{
					onError(response.error);
				}
			}, (error) => {
				onError(error);
			});
		})
			
			

		public check = (): ng.IPromise<IUser> => new this.$q((onSuccess, onError) => {
			this.restApiService.getData('check').then((response: RestApi.IResponse) => {
					if (response.success) {
						onSuccess(response.data);
					} else {
						onError(response.error);
					}
				}, (error) => {
					onError(error);
				});
		});
		
		
		private saveToken = (token: string) => {
			if(localStorage){
				localStorage.setItem(this.config.token.name, token);
			}
		}

		
	}
}