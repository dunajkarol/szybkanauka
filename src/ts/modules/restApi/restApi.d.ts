/// <reference path="restApiModule.ts" />
/// <reference path="services/restApiService.ts" />
/// <reference path="services/authService.ts" />


/// <reference path="controlers/authControler.ts" />


declare module RestApi {
		
	interface IResponse {
		success: boolean;
		data?: any;
		error?: IError;
	}
	
	interface IError {
		message: string;
		code: number;
		details?: any // tylko jeżeli jesteś adminem
	}
	
	interface IConfig {
		url?:string;
		host: string;
		token: {
			name: string;
			header: string;
		}
	}
	
	interface IResponseLogin extends IResponse {
		data: ResponseData.ILogin;
	}
	
	module ResponseData {
		interface ILogin {
			message: string;
			token: string;
		}
	}
}