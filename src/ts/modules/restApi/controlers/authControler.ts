/// <reference path="../../../common.d.ts" />

module RestApi {
    export class AuthController {
        //static $inject = ['AuthService'];

        private authService: RestApi.AuthService;

        constructor(authService: RestApi.AuthService) {
            this.authService = authService;
            this.test();
        }

        private test = () => {
            this.authService.check().then((res) => {
				console.log(res);
			});
        }
    }
}